package com.pajak.controllers;

import com.pajak.dto.RegisterUserData;
import com.pajak.dto.ResponseData;
import com.pajak.dto.ViewUserData;
import com.pajak.models.entities.User;
import com.pajak.services.UserService;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/users/register")
public class RegisterUserController {
    
    @Autowired
    private UserService userService;

    @Autowired
    private ModelMapper modelMapper;

    @PostMapping
    public ResponseEntity<?> register(@RequestBody RegisterUserData userData){
        ResponseData response = new ResponseData();
        try{
            User user = modelMapper.map(userData, User.class);
            // user.setEmail(userData.getEmail());
            // user.setPassword(userData.getPassword());
            // user.setFullName(userData.getFullName());

            userService.save(user);

            ViewUserData viewUserData = modelMapper.map(user, ViewUserData.class);
            // viewUserData.setEmail(user.getEmail());
            // viewUserData.setFullName(user.getFullName());
            // viewUserData.setId(user.getId());

            response.setPayload(viewUserData);
            response.setStatus(true);
            response.getMessages().add("User saved");
            return ResponseEntity.ok(response);
        }catch(Exception ex){
            response.setStatus(false);
            response.getMessages().add(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }
}
