package com.pajak.services;

import javax.transaction.Transactional;

import com.pajak.models.entities.App;
import com.pajak.models.repositories.AppRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class AppService implements UserDetailsService {

    @Autowired
    private AppRepository appRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return appRepository.findByUserName(username);
    }

    public App register(App app){
        return appRepository.save(app);
    }

    public Iterable<App> findAll(){
        return appRepository.findAll();
    }
    
}
