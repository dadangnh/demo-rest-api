package com.pajak.services;

import java.util.List;

import javax.transaction.Transactional;

import com.pajak.models.entities.Author;
import com.pajak.models.repositories.AuthorRepository;
import com.pajak.utilities.CSVHelper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
@Transactional
public class AuthorService {
    
    @Autowired
    private AuthorRepository authorRepository;

    public List<Author> save(MultipartFile file){
        try{
            List<Author> authors = CSVHelper.csvToAuthors(file.getInputStream());
            return authorRepository.saveAll(authors);
        }catch(Exception e){
            throw new RuntimeException("fail to store csv data "+e.getMessage());
        }
    }
}
