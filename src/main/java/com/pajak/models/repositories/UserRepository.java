package com.pajak.models.repositories;

import com.pajak.models.entities.User;

import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
    
    public User findByEmailAndPassword(String email, String password);
}
